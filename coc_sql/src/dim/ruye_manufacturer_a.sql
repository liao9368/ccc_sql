CREATE TABLE IF NOT EXISTS dim.ruye_manufacturer_a
(
    manufacturer_id          string      COMMENT '制造商ID',
    manufacturer_name        string      COMMENT '制造商名称',
    supplier_id              string      COMMENT '供应商ID',
    supplier_name            string      COMMENT  '供应商名称',
    manufacturer_desc        string      COMMENT  '制造商说明'
)
    comment '制造商信息'
    partitioned by (dt string)
    STORED AS PARQUET;

INSERT OVERWRITE TABLE dim.ruye_manufacturer_a PARTITION(dt)
SELECT
    a.supplier_manufacturer_id manufacturer_id,
    a.manufacturer_name manufacturer_name,
    a.supplier_id,
    b.supplier_name,
    a.manufacturer_desc,
    '${dt}'
from ods.ruye_srmtest_base_supplier_manufacturer_a  a
         left join dim.ruye_supplier_a b on b.dt = '${dt}' and a.supplier_id = b.supplier_code;